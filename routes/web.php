<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   return view('auth.login');
 });

// Route::get('/main', function(){
//     return view('layout.main');
// });

// Route::get('/master', function(){
//     return view('layout.master');
// });

//middleware
Route::group(['middleware' => ['auth']], function () {

  //CRUD 
//masuk ke create
Route::get('/activity/create', 'ActivityController@create');
Route::get('/category/create', 'CategoryController@create');

//kirim inputan table
Route::post('/activity', 'ActivityController@store');
Route::post('/category', 'CategoryController@store');


//READ data

//Tampil semua data
Route::get('/activity', 'ActivityController@index');
Route::get('/category', 'CategoryController@index');

//detail kolom
Route::get('/activity/{id}', 'ActivityController@show');
Route::get('/category/{id}', 'CategoryController@show');


//Update data

//masuk berdasarkan id
Route::get('/activity/{id}/edit', 'ActivityController@edit');
Route::get('/category/{id}/edit', 'CategoryController@edit');

//untuk kirim update berdasarkan idnya
Route::put('/activity/{id}', 'ActivityController@update');
Route::put('/category/{id}', 'CategoryController@update');


//Delete data 

//CRUD profile
Route::resource('/profile', 'profilecontroller')->only([
  'index','update'
]);


//Delete data  berdasarkan id
Route::delete('/activity/{id}', 'ActivityController@destroy');
Route::delete('/category/{id}', 'CategoryController@destroy');



//CRUD Activity
Route::resource('act', 'ActController');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
  
});







// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
