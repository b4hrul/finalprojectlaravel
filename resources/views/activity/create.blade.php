@extends('layout.master')

@section('judul')

Halaman Create Activity Order
@endsection

@section('content')  

<form action='/act' method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>Activity</label>
      <input type="text" name="activity" value="{{old('activity')}}" class="form-control">
    </div>
    
    @error('activity')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Time Order</label>
      <input type="text" name="time" value="{{old('time')}}" class="form-control">
    </div>
    
    @error('time')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror  

   <div class="form-group">
      <label>Status Closed/Open</label>
      <input type="text" name="status" value="{{old('status')}}" class="form-control">
    </div>
    
    @error('status')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Foto/Capture Alarm</label>
      <input type="file" name="foto" class="form-control" id="">      
    </div>
    
    @error('foto')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Category Activity</label>
      <select name="category_id" id="" class="form-control">
        <option value="">--Pilih Salah Satu kategori</option>
        @forelse ($category as $item)
            <option value="{{$item->id}}">{{$item->category}}</option>            
        @empty
            <option value="">tidak ada category</option>
        @endforelse
      </select>
    </div>
    
    @error('category_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror



  
    <button type="submit" class="btn btn-primary">Submit</button> 
  </form>

  @endsection




