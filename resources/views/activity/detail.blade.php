@extends('layout.master')

@section('judul')

Detail Activity Order
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <img src="{{asset('folderfoto/'. $activity->foto)}}" style="width: 100vh; heigh: 400px" class="card-img-top" alt="...">
            <div class="card-body">
                <h5>{{$activity->activity}}</h5>
                <p class="card-text">{{$activity->content,20}}</p>
                <a href="/act" class="btn btn-info btn-sm">kembali</a>
            </div>
        </div>
    </div>   
</div> 



@endsection




