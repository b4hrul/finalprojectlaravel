@extends('layout.master')

@section('judul')

Halaman List All Activity Order
@endsection

@section('content')
<a href="/act/create" class='btn btn-primary btn-sm mb-3'>Tambahkan Activity Order</a>

<div class="row">
    @forelse ($activity as $item)
        <div class="col-4">
            <div class="card" style="width: 18rem;">
                <img src="{{asset('folderfoto/'.$item->foto)}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5>{{$item->activity}}</h5>
                    <p class="card-text">{{ Str::limit($item->content,20)}}</p>
                    <form action="/act/{{$item->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="/act/{{$item->id}}" class="btn btn-info btn-sm">detail</a>
                        <a href="/act/{{$item->id}}/edit" class="btn btn-warning btn-sm">edit</a>
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form> 
                </div>
            </div>
        </div>        
    @empty
        <h1>Tidak ada activity</h1>        
    @endforelse
    
</div> 



@endsection




