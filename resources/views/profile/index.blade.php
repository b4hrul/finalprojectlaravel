@extends('layout.master')

@section('judul')

Halaman Update Profile
@endsection

@section('content')

<form action='/profile/{{$profile->id}}' method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" value="{{old('nama',$profile->nama)}}" class="form-control">
    </div>
    
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Role</label>
      
      <select id="name" type="text" class="form-control @error('role') is-invalid @enderror" name="role" value="{{ old('role',$profile->role) }}"  autofocus>
        <option>TS (technical support)</option>
        <option>TO (Technical operation)</option>
        <option>Telkomsel User</option>
        
    </select>
    </div>
    
    @error('role')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

   
    <button type="submit" class="btn btn-primary">Submit</button> 
  </form>


  @endsection  