
@extends('layout.master')

@section('judul')

Create Category
@endsection

@section('content')  

<form action='/category' method="POST">
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" value="{{old('nama')}}" class="form-control">
    </div>
    
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Role</label>
      <input type="text" name="role" value="{{old('role')}}" class="form-control">
    </div>
    
    @error('role')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

   
    <button type="submit" class="btn btn-primary">Submit</button> 
  </form>

  @endsection




