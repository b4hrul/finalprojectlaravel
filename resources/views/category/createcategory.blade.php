{{-- create data pemain film --}}

@extends('layout.master')

@section('judul')

Create Category
@endsection

@section('content')  

<form action='/category' method="POST">
    @csrf
    <div class="form-group">
      <label>Kategori</label>
      <input type="text" name="category" value="{{old('category')}}" class="form-control">
    </div>
    
    @error('category')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Description</label>
      <input type="text" name="description" value="{{old('description')}}" class="form-control">
    </div>
    
    @error('description')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

   
    <button type="submit" class="btn btn-primary">Submit</button> 
  </form>

  @endsection




