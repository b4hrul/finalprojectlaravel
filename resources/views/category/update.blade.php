@extends('layout.master')

@section('judul')

Halaman update category
@endsection

@section('content')  

<form action='/category/{{$category->id}}' method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Kategori</label>
      <input type="text" name="category" value="{{old('category',$category->category)}}" class="form-control">
    </div>
    
    @error('category')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Description</label>
      <input type="text" name="description" value="{{old('description',$category->description)}}" class="form-control">
    </div>
    
    @error('description')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

  @endsection