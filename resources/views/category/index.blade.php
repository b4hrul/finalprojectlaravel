@extends('layout.master')

@section('judul')

Halaman list category
@endsection

@section('content')
<a href="/category/create" class='btn btn-info btn-sm mb-4'>Tambahkan Category Activity</a>

<table class="table table-bordered">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Category</th>
        <th scope="col">Description</th>
      </tr>
    </thead>
    <tbody>
     @forelse ($category as $key =>$item)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$item->category}}</td>
            <td>{{$item->description}}</td>
            <td>
                <form action="/category/{{$item->id}}" method="POST">
                    @csrf
                    <a href="/category/{{$item->id}}" class='btn btn-info btn-sm'>detail</a>
                    <a href="/category/{{$item->id}}/edit" class='btn btn-warning btn-sm '>edit</a>
                    @method('delete')
                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
         
     @empty
         <tr>
            <td>tidak ada category</td>
         </tr>
     @endforelse
    </tbody>
</table>


  @endsection  