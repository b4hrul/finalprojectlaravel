<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Activity;
use File;

class ActController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activity = Activity::all();
        return view('activity.index',compact('activity'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('Activity.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'activity' => 'required',
            'time' => 'required',
            'status' => 'required',
            'category_id' => 'required',
            'foto' => 'required|mimes:pdf,xlx,csv,png,jpeg,jpg|max:2048',
        ]);
          
        $NamaFoto = time().'.'.$request->foto->extension();  
   
        $request->foto->move(public_path('folderfoto'), $NamaFoto);

        $activity = new Activity;

        $activity->activity = $request->activity; 
        $activity->time = $request->time; 
        $activity->status = $request->status; 
        $activity->category_id = $request->category_id; 
        $activity->foto = $NamaFoto; 

        $activity->save();

        return redirect('/act');
   
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $activity = Activity::find($id);
        return view('Activity.detail', compact('activity'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::all();
        $activity = Activity::find($id);
        return view('Activity.update', compact('activity','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'activity' => 'required',
            'time' => 'required',
            'status' => 'required',
            'category_id' => 'required',
            'foto' => 'mimes:pdf,xlx,csv,png,jpeg,jpg|max:2048',
        ]);

        $activity = Activity::find($id);
            if ($request->has('foto')) {

                $path = "folderfoto/";
                File::delete($path . $activity->foto);


                $NamaFoto = time().'.'.$request->foto->extension();        
        
                $request->foto->move(public_path('folderfoto'), $NamaFoto);

                $activity->foto = $NamaFoto;

                $activity->save();
            }

                $activity->activity = $request->activity; 
                $activity->time = $request->time; 
                $activity->status = $request->status; 
                $activity->category_id = $request->category_id; 

                $activity->save();

                return redirect('/act');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $activity = Activity::find($id);

        $path = "folderfoto/";
        File::delete($path . $activity->foto);
        
        $activity ->delete();

        return redirect('/act');


    }
}
