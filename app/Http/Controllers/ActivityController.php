<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ActivityController extends Controller
{
    public function create()
    {
        return view('activity.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        
        $request->validate([
            'activity' => 'required|min:6',
            'time' => 'required',
            'status' => 'required|min:3',
            'comment' => 'required|min:6',
           
        ],
        [
            'activity.required' => 'activity tidak boleh kosong harus diisi',
            'status.required' => 'status tidak boleh kosong harus diisi',
            'time.required' => 'time tidak boleh kosong harus diisi',
            'comment.required' => 'comment tidak boleh kosong harus diisi',        
            'activity.min' => 'activity min 3 char',        
            'comment.min' => 'comment min 6 char',     

        ]);

        DB::table('activity')->insert(
            [
                'activity' => $request['activity'], 
                'time' => $request['time'], 
                'status' => $request['status']          
            ]
        );
        return redirect('/activity');
    }

    // public function index()
    // {
    //     $activity = DB::table('activity')->get();
    //     //lihat tangkapan data
    //     //dd($activity);
    //     return view('activity.index', compact('activity'));
    // }

    // public function show($id)
    // {
    //     $activity = DB::table('activity')->where('id', $id)->first();

    //     return view('activity.detail', compact('activity'));
    // }

    // public function edit($id)
    // {
    //     $activity = DB::table('activity')->where('id', $id)->first();

    //     return view('activity.update', compact('activity'));
    // }

    // public function update(Request $request, $id)
    // {
    //     $request->validate([
    //         'activity' => 'required|min:6',
    //         'time' => 'required',
    //         'status' => 'required|min:6',
    //         'comment' => 'required|min:6'
    //     ],
    //     [
    //         'activity.required' => 'activity tidak boleh kosong harus diisi',
    //         'status.required' => 'status tidak boleh kosong harus diisi',
    //         'time.required' => 'time tidak boleh kosong harus diisi',
    //         'comment.required' => 'comment tidak boleh kosong harus diisi',        
    //         'activity.min' => 'activity min 6 char',        
    //         'comment.min' => 'comment min 6 char',      

    //     ]);

    //     DB::table('activity')
    //             ->where('id', $id)
    //             ->update(
    //                 [
    //                     'activity' => $request['activity'], 
    //                     'time' => $request['status'], 
    //                     'status' => $request['status'],              
    //                     'comment' => $request['comment']                
    //                 ]
    //             );
    //     return redirect('/activity');
    // }
    
    // public function destroy($id)
    // {
    //     DB::table('activity')->where('id', $id)->delete();

    //     return redirect('/activity');
    // }

}
