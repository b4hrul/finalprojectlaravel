<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function create()
    {
        return view('category.createcategory');
    }

    public function store(Request $request)
    {
        // dd($request->all());

        $request->validate([
            'category' => 'required|min:3',
            'description' => 'required|min:3',
        ],
        [
            'category.required' => 'Nama tidak boleh kosong harus diisi',
            'description.required' => 'umur tidak boleh kosong harus diisi',        
            'category.min' => 'nama min 3 char',        
            'description.min' => 'Bio min 3 char',     

        ]);

        DB::table('category')->insert(
            [
                'category' => $request['category'], 
                'description' => $request['description']
            ]
        );
        return redirect('/category');
    }

    public function index()
    {
        $category = DB::table('category')->get();
        //lihat tangkapan data
        //dd($category);
        return view('category.index', compact('category'));
    }

    public function show($id)
    {
        $category = DB::table('category')->where('id', $id)->first();

        return view('category.detail', compact('category'));
    }

    public function edit($id)
    {
        $category = DB::table('category')->where('id', $id)->first();

        return view('category.update', compact('category'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'category' => 'required|min:3',
            'description' => 'required|min:3',
        ],
        [
            'category.required' => 'Nama tidak boleh kosong harus diisi',
            'description.required' => 'umur tidak boleh kosong harus diisi',        
            'category.min' => 'nama min 3 char',        
            'description.min' => 'Bio min 3 char',     

        ]);

        DB::table('category')
                ->where('id', $id)
                ->update(
                    [
                        'category' => $request['category'],
                        'description' => $request['description']                  
                    ]
                );
        return redirect('/category');
    }
    
    public function destroy($id)
    {
        DB::table('category')->where('id', $id)->delete();

        return redirect('/category');
    }





}

