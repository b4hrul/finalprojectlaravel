<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\profile;


use Illuminate\Support\Facades\Auth;

class profilecontroller extends Controller
{
    
    public function index()
    {
        $id = Auth::id();
        $profile = profile::where('user_id',$id)->first();
        //dd($profile);
        return view('profile.index',compact('profile'));
    }
    
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'role' => 'required',
            
        ]);

        $profile = profile::find($id);

        $profile->nama = $request->nama;
        $profile->role = $request->role;

        $profile->save();

        return redirect('/profile');


    }

  

   
}
