<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'act';
    protected $fillable = ['activity', 'time', 'status', 'foto','category_id'];
}
